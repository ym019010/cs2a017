#include <stdlib.h>
#include <stdio.h>
int main(int argc, char *argv[])
{
    unsigned long page, offset, address;
    if(argc != 2)  exit(1);
    address= atoll(argv[1]);

    page = address >> 12;         /*calculating pages number*/
    offset = address & 0xfffff;     /*calculating remaining offset*/
    // changed 0xfff to 0xfffff. This is because 1MB == 1048576 in base 2 or 0xffffff in hex
    printf("The address %lu contains: \n", address);
    printf("page number = %lu\n",page);
    printf("offset = %lu\n", offset);
    return 0;
}
