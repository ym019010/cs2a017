#include <stdio.h>
#include<stdlib.h>
#include <unistd.h>
int main(void){

	int pfds[2]; // sort <======> cat
	int pfds1[2]; //cut <======> sort
	
	pipe(pfds);
	
	switch (fork()){
	case -1:
	perror("Fork");
	exit(1);
	
	case 0:
	pipe(pfds1);
	
		switch(fork()){
		case -1:
		perror("Fork");
		exit(2);
		
		case 0:
		dup2(pfds[1], STDOUT_FILENO);
		close(pfds1[0]);
		close(pfds1[1]);
		execl("/usr/bin/cat", "/usr/bin/cat", "/etc/group", NULL);
		exit(3);
		
		default:
		dup2(pfds1[0], STDIN_FILENO);
		dup2(pfds[1], STDOUT_FILENO);	
		close(pfds1[0]);
		close(pfds1[1]);
		execl("/usr/bin/sort", "/usr/bin/sort", NULL);
		exit(4);
		}
		default:
		dup2(pfds[0], STDIN_FILENO);
		close(pfds1[0]);
		close(pfds1[1]);
		execl("/usr/bin/cut", "/usr/bin/cut", "-f3", "-d:" ,NULL);
		exit(5);
             
             		
		}
	
	return 0;
}	