#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h>

struct my_msgbuf {
	long mtype;
	char mtext[200];
};

bool isCaps(char* msg)
{
	bool upc = false;
	bool lowc = false;
	for(int i = 0; i < msg[i] != '\0'; i++) {
		if (msg[i] >= 'a' && msg[i] <= 'z') {
		      lowc = true;
		} 
		else if (msg[i] >= 'A' && msg[i] <= 'Z') {
		      upc = true;
		}		
	}
	if (upc == true && lowc == false) {
		return true;
	} 
	else {
		return false;
	} 
}

int main(void)
{
	struct my_msgbuf buf;
	int msqid;
	key_t key;
	
	if ((key = ftok("kirk.c", 'B')) == -1) {
		perror("ftok");
		exit(1);
	}
	
	if(isUrgent(buf.mtext))  {
		if ((msqid = msgget(key, 0666 | IPC_CREAT)) == -1) {
			perror("msgget");
			exit(1);
		}
	}
	else {	
		if ((msqid = msgget(key, 0644 | IPC_CREAT)) == -1) {
			perror("msgget");
			exit(1);
	      	}
	}
	printf("Enter lines of text, ^D to quit:\n");

	buf.mtype = 1; /* we don't really care in this case */

	while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL) {
		int len = strlen(buf.mtext);

		/* ditch newline at end, if it exists */
		if (buf.mtext[len-1] == '\n') buf.mtext[len-1] = '\0';

		if (msgsnd(msqid, &buf, len+1, 0) == -1) /* +1 for '\0' */
			perror("msgsnd");
	}

	if (msgctl(msqid, IPC_RMID, NULL) == -1) {
		perror("msgctl");
		exit(1);
	}

	return 0;
}


