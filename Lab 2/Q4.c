#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(){
   int cp_1, cp_2;// child process 1 and 2
   int count = 0;
   (cp_1 = fork()) && (cp_2 = fork()); //creates 2 child processes 
   while(count < 10) //sets max to 10
   {
     count++; //raises counter by 1 so that starts at 1

    if (cp_1 == 0) //checks if child process
   {
       printf("Child %d process: counter = %d\n", getpid(), count); //outputs child process with id
    } 
     else if (cp_2 == 0) //checks if child process
 {
     printf("Child %d process: counter = %d\n", getpid(), count); //outputs child process with id
  } 
   else { //parent process
            printf("Parent %d process: counter = %d\n", getpid(), count); //outputs parent process with id
        }
        sleep(1); //end process after 1 second

   }
}

